# IO Test

Test IO throughput and latency to compare different filesystems. Output will look somewhat like:

```
$ ./iotest.py --files test /media/test/test2 --blocksizes 2k 4k 1M 2M 8M 16M

average disk Write/Read Latency in nanoseconds, ascending loops
############################################################################################
                                                               1.1ms  1000 writes [ecryptfs]  
                                                               0.5ms  1000 reads [ecryptfs]   
███                                                           11.3ms  1000 writes [fuse.sshfs]
██████████████████████████████████████████████████           148.0ms  1000 reads [fuse.sshfs] 
                                                               1.8ms  2000 writes [ecryptfs]  
                                                               0.7ms  2000 reads [ecryptfs]   
█                                                              3.7ms  2000 writes [fuse.sshfs]
██████████████████████                                        67.2ms  2000 reads [fuse.sshfs] 
                                                               1.6ms  3000 writes [ecryptfs]  
                                                               0.6ms  3000 reads [ecryptfs]   
█                                                              3.1ms  3000 writes [fuse.sshfs]
██████████████████                                            54.8ms  3000 reads [fuse.sshfs] 
                                                               1.6ms  4000 writes [ecryptfs]  
                                                               0.6ms  4000 reads [ecryptfs]   
█                                                              3.1ms  4000 writes [fuse.sshfs]
█████████████████                                             51.4ms  4000 reads [fuse.sshfs] 
                                                               1.5ms  5000 writes [ecryptfs]  
                                                               0.6ms  5000 reads [ecryptfs]   
                                                               2.7ms  5000 writes [fuse.sshfs]
████████████████                                              49.1ms  5000 reads [fuse.sshfs] 
Disk Read throughput in MB/s (32MB, different Blocksizes)         
######################################################################################
█████████████████████████████████                          3891.2MB/s  2k bs [ecryptfs]   
████████████████████████████████                           3840.0MB/s  2k bs [fuse.sshfs] 
██████████████████████████████████████████████             5529.6MB/s  4k bs [ecryptfs]   
████████████████████████████████████                       4300.8MB/s  4k bs [fuse.sshfs] 
██████████████████████████████████████████████             5427.2MB/s  1M bs [ecryptfs]   
█████████████████████████████████████████████████          5836.8MB/s  1M bs [fuse.sshfs] 
████████████████████████████████████████████████           5683.2MB/s  2M bs [ecryptfs]   
██████████████████████████████████████████████████         5888.0MB/s  2M bs [fuse.sshfs] 
█████████████████████████████████████████████              5324.8MB/s  8M bs [ecryptfs]   
██████████████████████████████████████████                 5017.6MB/s  8M bs [fuse.sshfs] 
████████████████████████████████████████                   4812.8MB/s  16M bs [ecryptfs]  
███████████████████████████████████████                    4608.0MB/s  16M bs [fuse.sshfs]
Disk Write throughput in MB/s (32MB, different Blocksizes)      
######################################################################################
███████████████████████████                                 167.5MB/s  2k bs [ecryptfs]   
██████████                                                   62.3MB/s  2k bs [fuse.sshfs] 
██████████████████████████████████████████████████          299.5MB/s  4k bs [ecryptfs]   
███████████                                                  66.9MB/s  4k bs [fuse.sshfs] 
███████████████████████████████████                         215.0MB/s  1M bs [ecryptfs]   
██████████                                                   64.1MB/s  1M bs [fuse.sshfs] 
██████████████████████████████████████                      228.5MB/s  2M bs [ecryptfs]   
██████████                                                   64.0MB/s  2M bs [fuse.sshfs] 
███████████████████████████████████                         210.0MB/s  8M bs [ecryptfs]   
███████████                                                  66.5MB/s  8M bs [fuse.sshfs] 
██████████████████████████████████                          208.5MB/s  16M bs [ecryptfs]  
██████                                                       40.3MB/s  16M bs [fuse.sshfs]
```

## Usage

```
usage: iotest.py [-h] [--blocksizes BLOCKSIZE [BLOCKSIZE ...]]
                 [--files FILE [FILE ...]]

Disk I/O test command line tool

optional arguments:
  -h, --help            show this help message and exit
  --blocksizes BLOCKSIZE [BLOCKSIZE ...]
                        blocksizes you want to test, default: [512, 1k, 4k, 64k, 512k, 1M, 4M, 16M, 56M, 128M, 256M, 512M, 1G] 
  --files FILE [FILE ...]
                        Temporary files to test disk I/O
```
