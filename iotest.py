#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ascii_graph import Pyasciigraph
import subprocess, re, sys, os, psutil, math, argparse
import numpy

# Helper functions
# Calculate total bytes from SI value (e.g. '1k' => 1024)
def toBytes(size='1k'):
    prefix = {'k': 1024, 'M': 1024 ** 2, 'G': 1024 ** 3, 'T': 1024 ** 4}
    num = re.search('[0-9]+\.?[0-9]*', size)
    try:
        num = float(num.group(0))
    except:
        num = 0
    pre = re.search('[kMGT]', size)
    try:
        return int(num * float(prefix[pre.group(0)]))
    except:
        return int(num)

# Calculate SI value from total bytes (e.g. 1024 => '1k')
def fromBytes(size=0):
    try:
        prefix = ['k', 'M', 'G', 'T']
        i = int(math.floor(math.log(size, 1024)))
        num = float(size)/float(1024 ** i)
        return '%12.4f%s' % (num,prefix[i-1])        
    except:
        return str(size)

def fromBytesInt(size=0):
    try:
        prefix = ['k', 'M', 'G', 'T']
        i = int(math.floor(math.log(size, 1024)))
        num = size/(1024 ** i)
        # print str(int(num)) + str(prefix[i-1])
        return '%s%s' % (num,prefix[i-1])        
    except:
        return str(size)

def human(line, unit):
    try:
        value = re.search(r'[0-9]+\.[0-9]+', line)
        after = re.search(r'\.[0-9]+', line)
        length = len(after.group(0))-2
        value = float(value.group(0))
    except:
        return line
    return re.sub(r'([0-9]+\.[0-9]+)', "%s%.1f%s"%(" "*length, value,unit), line, count=1)

# Get FS from file
def get_fs(path):
    path = os.path.abspath(path)
    while not os.path.ismount(path):
        path = os.path.dirname(path)
    fs=[x.fstype for x in partitions if x.mountpoint == path]
    return fs[0]

# Print process bar to cli
def progress(percent, text="Percent:", bar_length=30):
    percent = float(percent)/100
    hashes = u'█' * int(round(percent * bar_length))
    spaces = u' ' * (bar_length - len(hashes))
    sys.stdout.write("\r%s "%(text))
    sys.stdout.write(u"[{0}] {1}%                ".format(hashes + spaces, int(round(percent * 100))))
    sys.stdout.flush()

# Command line usage help + parse arguments
parser = argparse.ArgumentParser(description='Disk I/O test command line tool')
parser.add_argument('--blocksizes', help='blocksizes you want to test, default: [512, 1k, 4k, 64k, 512k, 1M, 4M, 16M, 56M, 128M, 256M, 512M, 1G]', type=str, metavar='BLOCKSIZE', nargs='+')
parser.add_argument('--files', required=True,metavar='FILE', type=str, nargs='+', help='Temporary files to test disk I/O')
args = parser.parse_args()

if args.blocksizes:
    blocksizes = args.blocksizes
    tmp_file_size = fromBytesInt(max([toBytes(x) for x in blocksizes ]))
else:
    blocksizes=['512', '1k', '4k', '64k', '512k', '1M', '4M', '16M', '56M', '128M', '256M', '512M', '1G']
    tmp_file_size = '2G'


tmp_files = args.files
partitions = psutil.disk_partitions(all=True)

# Estimate read/write latency
data = []
for i in [1000, 2000, 3000, 4000, 5000]:
    for tmp_file in tmp_files:
        p = subprocess.Popen(["dd", "if=/dev/zero", "of="+tmp_file, "bs=512", "count="+str(i)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out,err = p.communicate()
        time = re.search('[0-9]+\.[0-9]+ s', err)
        time = float(re.split(' ', time.group(0))[0])
        data.append((str(i) + ' writes ['+get_fs(tmp_file)+']', time/i*100000))

        p = subprocess.Popen(["dd", "of=/dev/null", "if="+tmp_file, "bs=512", "count="+str(i)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out,err = p.communicate()
        time = re.search('[0-9]+\.[0-9]+ s', err)
        time = float(re.split(' ', time.group(0))[0])
        data.append((str(i)+ ' reads ['+get_fs(tmp_file)+']', time/i*1000000))
        os.remove(tmp_file)
        progress(i/50)

graph = Pyasciigraph()
for line in graph.graph('\raverage disk Write/Read Latency in nanoseconds, ascending loops', data):
    # print(line)
    print(human(line, 'ms'))

# Prepare file to read from
counter=0
for tmp_file in tmp_files:
    progress(counter*100/len(tmp_files), text="Preparing...")
    counter=counter+1
    p = subprocess.Popen(["dd", "if=/dev/zero", "of="+tmp_file, "bs="+tmp_file_size, "count=2"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = p.communicate()
    progress(counter*100/len(tmp_files), text="Preparing...")

# Estimate read throughput
data = []
counter=0
for i in blocksizes:
    progress(counter*100/len(blocksizes), text="Read test")
    counter = counter+1
    for tmp_file in tmp_files:
        tmp = []
        for k in range(1,11):
            p = subprocess.Popen(["dd", "of=/dev/null", "if="+tmp_file, "bs="+i, "count="+str(toBytes(tmp_file_size)/toBytes(i))], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out,err = p.communicate()
            # time = re.search('[0-9]+\.[0-9]+ s', err)
            # time = float(re.split(' ', time.group(0))[0])
            mbs = re.search(', [0-9]+.[0-9] [kMG]?B', err)
            mbs = float(toBytes(mbs.group(0)[2:-1].replace(" ",""))) / (1024 ** 2)
            tmp.append(mbs)
        # data.append((i+ 'B blocksize on '+get_fs(tmp_file), time))
        data.append((i+ ' bs ['+get_fs(tmp_file)+ ']', numpy.percentile(tmp,50)))
        progress(counter*100/len(blocksizes), text="Read test")
for tmp_file in tmp_files:
    os.remove(tmp_file)
graph = Pyasciigraph()
for line in graph.graph('\rDisk Read throughput in MB/s ('+fromBytesInt(toBytes(tmp_file_size )*2)+'B, different Blocksizes)', data):
    print(human(line, 'MB/s'))


# Estimate write throughput
data = []
counter=0
for i in blocksizes:
    progress(counter*100/len(blocksizes), text="Write test")
    counter = counter+1
    for tmp_file in tmp_files:
        tmp = []
        for k in range(1,11):
            p = subprocess.Popen(["dd", "if=/dev/zero", "of="+tmp_file, "bs="+i, "count="+str(toBytes(tmp_file_size)/toBytes(i))], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out,err = p.communicate()
            # time = re.search('[0-9]+\.[0-9]+ s', err)
            # time = float(re.split(' ', time.group(0))[0])
            mbs = re.search(', [0-9]+.[0-9] [kMG]?B', err)
            mbs = float(toBytes(mbs.group(0)[2:-1].replace(" ",""))) / (1024 ** 2)
            tmp.append(mbs)
        # data.append((i + 'B blocksize on '+get_fs(tmp_file), time))
        # data.append((i + ' bs ['+get_fs(tmp_file)+']', numpy.percentile(tmp, 75)))
        data.append((i + ' bs ['+get_fs(tmp_file)+']', numpy.percentile(tmp, 50)))
        # data.append((i + ' bs ['+get_fs(tmp_file)+']', numpy.percentile(tmp, 25)))
        # data.append((' [%.1f %.1f] '%(numpy.percentile(tmp, 25), numpy.percentile(tmp, 75)) + i + ' bs ['+get_fs(tmp_file)+']', numpy.percentile(tmp, 50)))
        os.remove(tmp_file)
        progress(counter*100/len(blocksizes))
        progress(counter*100/len(blocksizes), text="Write test")
graph = Pyasciigraph()
for line in graph.graph('\rDisk Write throughput in MB/s ('+ fromBytesInt(toBytes(tmp_file_size )*2)+'B, different Blocksizes)', data):
    print(human(line, 'MB/s'))

